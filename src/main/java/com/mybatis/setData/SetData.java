package com.mybatis.setData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import com.mybatis.bean.Employee;


public class SetData {
	private static List<Employee> EmployeeData = new ArrayList<Employee>();
	public List<Employee> createEmployeeData() throws IOException {
		
	try {
		String string;
		int ID = 1;
		// read data and split and set Data in list
		BufferedReader br = new BufferedReader(new FileReader("D:\\list_error.txt"));
		while ((string = br.readLine()) != null) {
			String[] tokens = string.split(" ");


			Employee temp = new Employee(ID, tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5],
					computBMI(Float.parseFloat(tokens[0]), Float.parseFloat(tokens[1])));
			EmployeeData.add(temp);
			ID++;
		}
		br.close();
		
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	return EmployeeData;
}
public static float computBMI(float height, float weight) {
	float BMI;
	BMI = weight / ((height / 100) * (height / 100));
	return BMI;
}


}
