package com.mybatis.app;

import com.mybatis.setData.SetData;
import com.mybatis.test.JdbcTest;
import com.mybatis.test.MybatisTest;

public class MybatisApp {

	public static void main(String[] args) {
		try {
			MybatisTest test = new MybatisTest();
			JdbcTest jtest = new JdbcTest();
			SetData setData =new SetData();
			//test.findById(1);
			//test.insert();
			//test.update();
			//test.delete(1);
			//setData.createEmployeeData();
			//test.batchinsert(setData.createEmployeeData());
			//test.download();
			jtest.jdbcBatchInsert();
			//jtest.jdbcDownload();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
