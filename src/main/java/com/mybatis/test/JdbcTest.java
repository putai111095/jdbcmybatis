package com.mybatis.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mybatis.bean.Employee;
import com.mybatis.setData.SetData;

public class JdbcTest {
	Logger logger = LoggerFactory.getLogger(JdbcTest.class);
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	// static final String DB_URL =
	// "jdbc:mysql://127.0.0.1:3306/emp_diego?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static final String DB_URL = "jdbc:mysql://10.67.67.186:3306/emp_diego?characterEncoding=utf8&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2b8";
	static final String USER = "root";
	static final String PASS = "123456";
	static final String insertSQl = "INSERT INTO hw2 (ID,Height,Weight,ENGname,CHNname,Ext,Email,BMI) VALUES (?,?,?,?,?,?,?,?)";
	static final String downloadSQL = "SELECT * FROM hw2";

	private static void doRollback(Connection c) {
		try {
			c.rollback();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public void jdbcBatchInsert() throws IOException {
		SetData setData = new SetData();
		List<Employee> employee = setData.createEmployeeData();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			PreparedStatement ps = conn.prepareStatement(insertSQl);
			conn.setAutoCommit(false);
			int count = 0 ;
			for (Employee emp : employee) {
				ps.setInt(1, emp.getId());
				ps.setString(2, emp.getHeight());
				ps.setString(3, emp.getWeight());
				ps.setString(4, emp.getengName());
				ps.setString(5, emp.getchnName());
				ps.setString(6, emp.getExt());
				ps.setString(7, emp.getEmail());
				ps.setFloat(8, emp.getBMI());
				ps.addBatch();
				if (++count % 10 == 0) {
					ps.executeBatch(); 
				}
			}
			System.out.println("success insert");
			ps.executeBatch();

		} catch (SQLException e) {
			doRollback(conn);
			try {
				conn.setAutoCommit(true);
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}

	}

	public void jdbcDownload() throws FileNotFoundException {
		ResultSet resultSet = null;
		Connection conn = null;
		PrintWriter writer = new PrintWriter(new File("test.csv"));
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			StringBuilder sb = new StringBuilder();
			Statement stmt = conn.createStatement();
			resultSet = stmt.executeQuery(downloadSQL);
			while (resultSet.next()) {
				sb.append(resultSet.getInt(2)).append(",");
				sb.append(resultSet.getString(3)).append(",");
				sb.append(resultSet.getString(4)).append(",");
				sb.append(resultSet.getString(5)).append(",");
				sb.append(resultSet.getString(6)).append(",");
				sb.append(resultSet.getString(7)).append(",");
				sb.append(resultSet.getString(8)).append(",");
				sb.append(resultSet.getFloat(9)).append("\n");
			}
			writer.write(sb.toString());
			System.out.println(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}

	}
}
