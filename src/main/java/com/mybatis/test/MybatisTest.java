package com.mybatis.test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.mybatis.bean.Employee;
import com.mybatis.mapper.MybatisMapper;

public class MybatisTest {

	private static SqlSessionFactory sqlSessionFactory;
	private static Reader reader;

	static {
		try {
			reader = Resources.getResourceAsReader("configuration.xml");
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SqlSessionFactory getSession() {
		return sqlSessionFactory;
	}

	public void findById(int id) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			MybatisMapper mybatisMapper = session.getMapper(MybatisMapper.class);
			Employee employee = mybatisMapper.findById(id);
			System.out.println(employee);

		} finally {
			session.close();
		}
	}

	public void insert() {
		Employee employee = new Employee();
		employee.setId(80);
		employee.setHeight("185");
		employee.setWeight("80");
		employee.setengName("abc");
		employee.setchnName("源");
		employee.setExt("1234");
		employee.setEmail("pt@gmail.com");
		employee.setBMI((float) 10.12);
		SqlSession session = sqlSessionFactory.openSession();
		try {
			MybatisMapper mybatisMapper = session.getMapper(MybatisMapper.class);
			mybatisMapper.insert(employee);
			session.commit();
			System.out.println("新增用戶：" + employee);
		} finally {
			session.close();
		}
	}

	public void delete(int id) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			MybatisMapper mybatisMapper = session.getMapper(MybatisMapper.class);
			mybatisMapper.delete(id);
			session.commit();
			System.out.println("刪除成功");
		} finally {
			session.close();
		}
	}

	public void update() {
		Employee employee = new Employee();
		employee.setId(80);
		employee.setHeight("184");
		employee.setWeight("80");
		employee.setengName("abc");
		employee.setchnName("源");
		employee.setExt("1234");
		employee.setEmail("pt@gmail.com");
		employee.setBMI((float) 10.12);
		SqlSession session = sqlSessionFactory.openSession();
		try {
			MybatisMapper mybatisMapper = session.getMapper(MybatisMapper.class);
			mybatisMapper.update(employee);
			session.commit();
			System.out.println("更新用戶：" + employee);
		} finally {
			session.close();
		}
	}

	public void batchinsert(List<Employee> employee) {
		System.out.println(employee.size());
		SqlSession session = sqlSessionFactory.openSession();
		try {
			int size = employee.size();
			for (int i = 1; i <= size; i++) {
				MybatisMapper mybatisMapper = session.getMapper(MybatisMapper.class);
				if (i % 10 == 0) {
					mybatisMapper.batchInsert(employee.subList(i - 10, i));
					System.out.println(i);
					System.out.println("每10筆提交");
					session.commit();
					session.clearCache();
				} else if (i == size) {
					mybatisMapper.batchInsert(employee.subList(50, i));
					System.out.println(i);
					System.out.println("提交剩下");
					session.commit();
					session.clearCache();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.rollback();
		} finally {
			session.close();
		}

	}

	public void download() throws IOException {
		SqlSession session = sqlSessionFactory.openSession();
		PrintWriter writer = new PrintWriter(new File("test.csv"));
		try {
			MybatisMapper mybatisMapper = session.getMapper(MybatisMapper.class);
			StringBuilder sb = new StringBuilder();
			for (Employee emp : mybatisMapper.findAll()) {
				sb.append(emp.getDataID()).append(",");
              sb.append(emp.getId()).append(",");
              sb.append(emp.getHeight()).append(",");
              sb.append(emp.getWeight()).append(",");
              sb.append(emp.getengName()).append(",");
              sb.append(emp.getchnName()).append(",");
              sb.append(emp.getExt()).append(",");
              sb.append(emp.getEmail()).append(",");
              sb.append(emp.getBMI()).append("\n");
			}
			 writer.write(sb.toString());
		} finally {
			session.close();
			writer.close();
		}
	}
}
