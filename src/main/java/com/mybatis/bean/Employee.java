package com.mybatis.bean;
import java.sql.Date;

public class Employee {
	
		private int dataID;
		private int id;
		private String height;
		private String weight;
		private String engName;
		private String chnName;
		private String ext ;
		private String email;
		private float bmi;
		private Date createtime;
		private Date updatetime;
		
		public Employee() {
			
		}
		public Employee(int Id,String Height,String Weight,String EName,String CName,String Ext,String Email,float BMI) {
			this.id=Id;
			this.height=Height;
			this.weight=Weight;
			this.engName=EName;
			this.chnName=CName;
			this.ext=Ext;
			this.email=Email;
			this.bmi=BMI;
		}
		public float getBMI() {
			return bmi;
		}
		public void setBMI(float bMI) {
			bmi = bMI;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getHeight() {
			return height;
		}
		public void setHeight(String height) {
			this.height = height;
		}
		public String getWeight() {
			return weight;
		}
		public void setWeight(String weight) {
			this.weight = weight;
		}
		public String getengName() {
			return engName;
		}
		public void setengName(String eName) {
			this.engName = eName;
		}
		public String getchnName() {
			return chnName;
		}
		public void setchnName(String cName) {
			this.chnName = cName;
		}
		public String getExt() {
			return ext;
		}
		public void setExt(String ext) {
			this.ext = ext;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String toString() {
			return id+ "," +height  + "," + weight + "," + engName +"," + chnName +"," + ext +"," + email +"," + bmi + ","+createtime+","+updatetime+"\n";
		}
		public Date getCreatetime() {
			return createtime;
		}
		public void setCreatetime(Date createtime) {
			this.createtime = createtime;
		}
		public Date getUpdatetime() {
			return updatetime;
		}
		public void setUpdatetime(Date updatetime) {
			this.updatetime = updatetime;
		}
		public int getDataID() {
			return dataID;
		}
		public void setDataID(int dataID) {
			this.dataID = dataID;
		}
		
	}
