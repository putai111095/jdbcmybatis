package com.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.mybatis.bean.Employee;

public interface MybatisMapper {
	 Employee findById(int id);
	 int insert(Employee employee); 
	 int update(Employee employee); 
	 int delete(int id);
	 void batchInsert(@Param("employeeList")List<Employee>list);
	 List<Employee> findAll();
}
